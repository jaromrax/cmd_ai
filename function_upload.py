#!/usr/bin/env python3

from cmd_ai import config
from cmd_ai.version import __version__
import json

"""

"""
import time

from fire import Fire
import datetime as dt
import requests
from bs4 import BeautifulSoup
from selenium import webdriver


from selenium.webdriver.firefox.service import Service as FirefoxService
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from webdriver_manager.firefox import GeckoDriverManager

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service
from subprocess import getoutput



from selenium.webdriver.common.by import By

from console import fg


def upload_file(filename, url="https://photobooth.online/en-us/photo-restoration"):
    res = "No information  is available now."

    options = Options()
    #firefox_options = FirefoxOptions()
    #firefox_options.add_argument("--headless")  # Run in headless mode


    options.binary_location = getoutput("find /snap/firefox -name firefox").split("\n")[-1]
    options.add_argument("--headless")  # Run in headless mode

    driver = webdriver.Firefox(service = Service(executable_path = getoutput("find /snap/firefox -name geckodriver").split("\n")[-1]),    options = options)
    driver.get(url)

    element = driver.find_element(By.ID, "examples-upload-photo-button")

    #element = driver.find_element_by_class_name( "upload-photo-button_buttonText_mobile__t08aG" )
    print(element)
    print( element.text )

    element.click()
    time.sleep(1)
    element.send_keys( filename )

    page_source = driver.page_source


    driver.quit()


if __name__=="__main__":
    Fire(upload_file)
