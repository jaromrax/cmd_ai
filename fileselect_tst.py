#!/usr/bin/env python3

import os
from simple_term_menu import TerminalMenu

def select_file():
    files = sorted([f for f in os.listdir('.') if os.path.isfile(f)], key=str.casefold)
    shortcuts = list(map(str, range(1, 10))) + list("abcdefghijklmnopqrstuvwxyz")
    menu_entries = [f"[{shortcuts[i]}] {file}" for i, file in enumerate(files) if i < len(shortcuts)]
    terminal_menu = TerminalMenu(menu_entries, title="Select a file:")
    menu_entry_index = terminal_menu.show()
    if menu_entry_index is None:
        print("No file selected.")
    else:
        selected_file = files[menu_entry_index]
        print(f"Selected file: {selected_file}")

if __name__ == "__main__":
    select_file()

# import os
# from simple_term_menu import TerminalMenu

# def select_file():
#     files = sorted([f for f in os.listdir('.') if os.path.isfile(f)], key=str.casefold)
#     terminal_menu = TerminalMenu(files, title="Select a file:")
#     menu_entry_index = terminal_menu.show()
#     if menu_entry_index is None:
#         print("No file selected.")
#     else:
#         selected_file = files[menu_entry_index]
#         print(f"Selected file: {selected_file}")

# if __name__ == "__main__":
#     select_file()


# import curses
# import os

# def select_file(stdscr):
#     curses.curs_set(0)  # Hide the cursor
#     current_selection = 0
#     files = os.listdir('.')
#     files = [file for file in files if os.path.isfile(file)]  # Filter out directories

#     while True:
#         stdscr.clear()
#         height, width = stdscr.getmaxyx()
#         for idx, file in enumerate(files):
#             if idx == current_selection:
#                 stdscr.attron(curses.A_REVERSE)
#                 stdscr.addstr(idx % height, 0, f"{file}")
#                 stdscr.attroff(curses.A_REVERSE)
#             else:
#                 stdscr.addstr(idx % height, 0, file)

#         key = stdscr.getch()

#         if key == curses.KEY_UP and current_selection > 0:
#             current_selection -= 1
#         elif key == curses.KEY_DOWN and current_selection < len(files) - 1:
#             current_selection += 1
#         elif key in [curses.KEY_ENTER, ord('\n')]:
#             break

#         stdscr.refresh()

#     return files[current_selection]

# def main():
#     selected_file = curses.wrapper(select_file)
#     print(f"Selected file: {selected_file}")
#     print(" as das  sasdsagrw ewf rw we few")

# if __name__ == "__main__":
#     main()
