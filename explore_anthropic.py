#!/usr/bin/env python3

from fire import Fire
import anthropic
from cmd_ai.texts import role_assistant
from console import fg, bg
from cmd_ai.api_key import get_api_key_anthropic
import json

def decode_response_anthropic( response):
    """
    [TextBlock(text="I don't have access to real-time information. I can't tell you the current time.", type='text')]
    """
    #print(type(response))
    return response[0].text



def main( prompt, , temp=0, model="claude-3-5-sonnet-20241022", max_tokens=1000, role=None):

    # ** init
    client = anthropic.Anthropic(api_key=get_api_key_anthropic())

    system_prompt = role_assistant
    if role is None:
        system_prompt = role_assistant
    #********************************************** GO
    message = client.messages.create(
        model=model,
        max_tokens=max_tokens,
        temperature=temp,
        system=system_prompt,
        messages=[
            {
                "role": "user", "content": [ {"type": "text", "text": prompt }]
            } #, {next turn}, {next turn}

        ]
    )
    # **********
    response = decode_response_anthropic(message.content)
    print(fg.yellow, ">", response, fg.default)


if __name__ == "__main__":
    Fire(main)
